import axios from 'axios';
import { THUMBNAIL_LIST_URL } from '../config/apiUrl.js'
 
export default function SearchService(searchKey) {
   const url = `${THUMBNAIL_LIST_URL}?query=${searchKey}`
   return axios.get(url);  
}

