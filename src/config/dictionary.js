export const Dictionary = {
    searchBox: {
        label: 'Search Here'
    },
    loading: 'loading...',
    noResultsFound: 'No results found.'
}