export function thumbnailGroupNameGenerator(name) {
    const formattedName = name.split('.')[0].slice(8).replaceAll("-", " ").trim(); 
    const from = name.slice(0,8).split("-");
    const dateMMDDYY = new Date(from[2], from[1] - 1, from[0]);
  return [formattedName, dateMMDDYY];
}

export function randomNameGenerator(list) {
  const valueBetweenOneAndTen = Math.round(Math.random() * 10);
  return list[valueBetweenOneAndTen];
}