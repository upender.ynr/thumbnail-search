import React, { useEffect, useState , useCallback} from "react";
import Input from "../components/Input";
import SearchService from "../services/SearchService";
import PropTypes from "prop-types";
import { Dictionary } from '../config/dictionary';
import 'bootstrap/dist/css/bootstrap.css';
import '../styles/containers/landing.css';
import ThumbnailList from "../components/ThumbnailList";
import { debounce } from "lodash";

export default function Landing(props) {

  const [formattedDataForThumbnails, setFormattedDataForThumbnails] = useState();
  const [errorMessage, setErrorMessage] = useState('');
  const [message, setMessage] = useState('');

  const handler = useCallback(debounce(searchThumbnails, 700), []);

  function inputHandler(event) {
    handler(event.target.value);
  }

  function searchThumbnails(searchKey) {
    setMessage(Dictionary.loading)
    SearchService(searchKey).then((res)=>JSON.parse(res.data.contents)).catch((e)=>setErrorMessage('There is some problem')).then(res=>formatImagesUrlIntoSetOfThree(res.data)).then(res=>{ setMessage(''); if(res.length===0) {setMessage(Dictionary.noResultsFound)} else {setMessage('')} return res}).then(res=>setFormattedDataForThumbnails(res))
  }

  function formatImagesUrlIntoSetOfThree(data) {
    
    return data.reduce(function(acc, elem, i){
        if(i%3===0) {
          acc.push({
            images: {
            one: {
              url: data[i] ? data[i].imageUrl: '',
              name: data[i] ? data[i].name : ''
            },
            two: {
              url:  data[i+1] ? data[i + 1].imageUrl: '',
              name:  data[i+1] ? data[i + 1].name : ''
            },
            three: {
              url:  data[i+2] ? data[i + 2].imageUrl : '',
              name:  data[i+2] ? data[i + 2].name : ''
            }
          }
          })
          return acc
        } else return acc
        
      },[])
  }


  return (
        <div className='container row'>
          <Input
              id='search'
              name='search'
              className='search-input col-md-3 col-lg-3 col-xs-12'
              label={Dictionary.searchBox.label}
              handler ={inputHandler}
              type='text'
              isError={errorMessage!==''}
              errorMessage={errorMessage}
          >
          </Input>
         {message ? <div>{message}</div> : (formattedDataForThumbnails && <ThumbnailList
            list = {formattedDataForThumbnails}
            className='col-md-9 col-lg-9 col-xs-12'
            
          >
          </ThumbnailList>)
        }
        </div>
  );
}

Landing.propTypes = {
  Dictionary: PropTypes.object,
  inputHandler: PropTypes.func,
  formatImagesUrlIntoSetOfThree: PropTypes.func
};