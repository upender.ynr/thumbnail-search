import React from "react";
import PropTypes from "prop-types";
import Thumbnail from "./Thumbnail";
import '../styles/components/thumbnailList.css'

export default function ThumbnailList(props) {

  return (
      <div className={'thumnail-list ' + props.className}>
        {props.list.length>0 && props.list.map(function mapThumbnailImages(elem, i){   
        return <Thumbnail 
                  key={elem.images.one.url}
                  id={elem.images.one.name}
                  className={'someClass'}
                  images={elem.images}
                ></Thumbnail>
        })} 
      </div> 
      );
  }

  ThumbnailList.propTypes = {
    list: PropTypes.any
  }