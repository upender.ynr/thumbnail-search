import React from "react";
import PropTypes from "prop-types";
import '../styles/components/thumbnail.css';
import { randomNameGenerator, thumbnailGroupNameGenerator } from '../helpers/helpers'
import { NAMES_LIST } from "../__mocks__/__mocks__";
import moment from "moment";

export default function Thumbnail(props) {
  
  const firstImageName = props.images.one.name;
  const [thumbnailGroupName, dateMMDDYY] = thumbnailGroupNameGenerator(firstImageName);
  const sharedByName = randomNameGenerator(NAMES_LIST);
  const dateInWords = moment(dateMMDDYY).format('DD MMMM YYYY')
          
  return (
      <div className={'thumbnail-group ' + props.className} id={props.id}>
        <img className='image-one' alt={props.images.one.name} src={props.images.one.url}></img>
        <div className= 'second-row'>
          <img  className='image-two' alt={props.images.two.name} src={props.images.two.url}></img>
          <img className='image-three' alt={props.images.three.name} src={props.images.three.url}></img>
        </div>
        
        <div className='row thumbnail-name'>{thumbnailGroupName.toUpperCase()}</div>
        <div className='row shared-by'><pre>Shared by </pre><span className='name-highlighter'>{sharedByName}</span><pre> on </pre><span className='name-highlighter'>{dateInWords}</span></div>
     </div>     
    );
  }

  Thumbnail.propTypes = {
    id: PropTypes.string,
    images: PropTypes.object,
  }